using System;
using System.Windows.Forms;

namespace Calculadora2
{
    public partial class Calculadora2 : Form
    {

        //Jhustin Ismael Arias Perez 4-A T/M

        public Calculadora2()
        {
            InitializeComponent();
        }

        private void calculo_Click(object sender, EventArgs e)
        {
         
            double salario = cuenta * inversion_s;
            double sueldoTotal, UMA, importacion, IMSS;
            double dia_contado = Convert.ToDouble(txtDias.Text);
            double inversion_s = Convert.ToDouble(txtDiario.Text);

            lblSDI.Text = salario.ToString();
            bool contado = false;
            UMA = Convert.ToDouble(lblUMA.Text);

            if (inversion_s * 30 < 6000)
            {
                contado = true;
            }
            else
            {
                contado = false;
                subsidioEm = 0;
                lblSub.Text = subsidioEm.ToString();
            }



            if (salario >= 0.01 || salario <= 578.52)
            {
                excedente = salario - 0.01;
                excedente *= 0.0192; // 1.92%
                cuotaFija = 0;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salario - ISR;
                lblISR.Text = ISRTotal.ToString();

                if ((UMA * 3) < salario)
                {
                    diferencia = salario - (UMA * 3);
                    importacion = (diferencia * dia_contado) * 0.004;//0.40%
                    prestacionesD = salario * dia_contado * 0.0025;//25%
                    prestacionesE = salario * dia_contado * 0.00375;
                    inVida = salario * dia_contado * 0.00625;
                    vejez = salario * dia_contado * 0.01125;
                    IMSS = diferencia + importacion + prestacionesD + prestacionesE + inVida + vejez;

                    if (contado == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 && totalPercepciones < 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 && totalPercepciones < 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
    
                    }



                }

            }
            else if (salario >= 578.53 || salario <= 4910.18)
            {
                excedente = salario - 578.53;
                excedente *= 0.064;//6.40%
                cuotaFija = 11.11;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salario - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario)
                {
                    diferencia = salario - (UMA * 3);
                    importacion = (diferencia * dia_contado) * 0.004;//0.0040%
                    prestacionesD = salario * dia_contado * 0.0025;
                    prestacionesE = salario * dia_contado * 0.00375;
                    inVida = salario * dia_contado * 0.00625;
                    vejez = salario * dia_contado * 0.01125;
                    IMSS = diferencia + importacion + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    totalDeducciones = ISRTotal + IMSS;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (contado == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
    
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
     
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
 
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                }
            }
            else if (salario >= 4910.19 || salario <= 8629.20)
            {
                excedente = salario - 4910.19;
                excedente *= 0.1088;//10.88%
                cuotaFija = 288.33;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salario - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario)
                {
                    diferencia = salario - (UMA * 3);
                    importacion = (diferencia * dia_contado) * 0.004;//0.0040%
                    prestacionesD = salario * dia_contado * 0.0025;
                    prestacionesE = salario * dia_contado * 0.00375;
                    inVida = salario * dia_contado * 0.00625;
                    vejez = salario * dia_contado * 0.01125;
                    IMSS = diferencia + importacion + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    totalDeducciones = ISRTotal + IMSS;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (contado == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
     
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();

                        }
                }
            }
            else if (salario >= 8629.21 || salario <= 10031.07)
            {
                excedente = salario - 8629.21;
                excedente *= 0.16;//%16;
                cuotaFija = 692.96;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salario - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario)
                {
                    diferencia = salario - (UMA * 3);
                    importacion = (diferencia * dia_contado) * 0.004;//0.0040%
                    prestacionesD = salario * dia_contado * 0.0025;
                    prestacionesE = salario * dia_contado * 0.00375;
                    inVida = salario * dia_contado * 0.00625;
                    vejez = salario * dia_contado * 0.01125;
                    IMSS = diferencia + importacion + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    totalDeducciones = ISRTotal + IMSS;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (contado == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2632.66 || totalPercepciones <= 3071.4)
                        {
                            subsidioEm = 145.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3510.16 || totalPercepciones <= 3642.6)
                        {
                            subsidioEm = 107.4;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 3642.61)
                        {

                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                }
            }
            else if (salario >= 10031.08 || salario <= 12009.94)
            {
                excedente = salario - 10031.08;
                excedente *= 0.1792;//17.92%
                cuotaFija = 917.26;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salario - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario)
                {
                    diferencia = salario - (UMA * 3);
                    importacion = (diferencia * dia_contado) * 0.004;//0.0040%
                    prestacionesD = salario * dia_contado * 0.0025;
                    prestacionesE = salario * dia_contado * 0.00375;
                    inVida = salario * dia_contado * 0.00625;
                    vejez = salario * dia_contado * 0.01125;
                    IMSS = diferencia + importacion + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    totalDeducciones = ISRTotal + IMSS;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (contado == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1309.21 || totalPercepciones <= 1713.6)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 1745.71 || totalPercepciones <= 2193.75)
                        {
                            subsidioEm = 188.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                        else if (totalPercepciones >= 3071.41 || totalPercepciones <= 3510.15)
                        {
                            subsidioEm = 1251.1;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
   
                        else if (totalPercepciones >= 3642.61)
                        {
                            subsidioEm = 0;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                }
            }
            else if (salario >= 12009.95 || salario <= 24222.31)
            {
                excedente = salario - 12009.95;
                excedente *= 0.2126;//21.36%
                cuotaFija = 1271.87;
                excedente += cuotaFija;
                ISR = cuotaFija + excedente;
                ISRTotal = salario - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < salario)
                {
                    diferencia = salario - (UMA * 3);
                    importacion = (diferencia * dia_contado) * 0.004;//0.0040%
                    prestacionesD = salario * dia_contado * 0.0025;
                    prestacionesE = salario * dia_contado * 0.00375;
                    inVida = salario * dia_contado * 0.00625;
                    vejez = salario * dia_contado * 0.01125;
                    IMSS = diferencia + importacion + prestacionesD + prestacionesE + inVida + vejez;
                    lblIMSS.Text = IMSS.ToString();
                    totalDeducciones = ISRTotal + IMSS;
                    lblDeducciones.Text = totalDeducciones.ToString();
                    sueldoTotal = totalPercepciones - totalDeducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (contado == true)
                    {

                        if (totalPercepciones >= 0.01 || totalPercepciones <= 872.85)
                        {
                            subsidioEm = 200.85;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 872.86 || totalPercepciones <= 1309.2)
                        {
                            subsidioEm = 200.7;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                        else if (totalPercepciones >= 1713.61 || totalPercepciones <= 1745.7)
                        {
                            subsidioEm = 193.8;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
    
                        }
                        else if (totalPercepciones >= 2193.76 || totalPercepciones <= 2327.55)
                        {
                            subsidioEm = 174.75;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalPercepciones >= 2327.56 || totalPercepciones <= 2632.65)
                        {
                            subsidioEm = 160.35;
                            lblSub.Text = subsidioEm.ToString();
                            sueldoTotal += subsidioEm;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }

                    }
                }
            }

 